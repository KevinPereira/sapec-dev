<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_an_email_to_commercial_when_a_expense_approved</fullName>
        <description>Send an email when a expense approved - to the Commercial/Coordinator and CC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Brazil_Customer_Care</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tradecorp_Brazil/Brazil_Despesa_Aprovada</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_to_commercial_when_a_expense_is_rejected</fullName>
        <description>Send an email when a expense is rejected - to the Commercial/Coordinator</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tradecorp_Brazil/Brazil_Despesa_Rejeitada</template>
    </alerts>
    <fieldUpdates>
        <fullName>Expense_update_record_type_locked</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Brazil_Normal_Locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Expense update record type locked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Expense_update_record_type_open</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Brazil_Normal</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Expense update record type open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status_Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_InApproval</fullName>
        <field>Status__c</field>
        <literalValue>In Approval</literalValue>
        <name>Update Status_InApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Open</fullName>
        <field>Status__c</field>
        <literalValue>Open</literalValue>
        <name>Update Status_Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status_Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Brasil_Update_Manager</fullName>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
