<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>australia opportunities %3A make global price contain GST and Freight</fullName>
        <active>true</active>
        <formula>OR( RecordType.DeveloperName = &quot;Australia_Commercial_Quote&quot;, RecordType.DeveloperName = &quot;Australia_Quote_Approved_Record_Locked&quot;,
RecordType.DeveloperName = &quot;Australia_Quote_In_Approval&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>bonification opportunity %3A make global price equal to zero</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Quote.is_Bonus__c</field>
            <operation>equals</operation>
            <value>Free Goods Item</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
