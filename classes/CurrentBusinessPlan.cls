public with sharing class CurrentBusinessPlan {
	private List<Business_Plan_Line__c> cbplan_lines;
	private Account acc;
	public CurrentBusinessPlan(ApexPages.StandardController controller) {
		this.acc= (Account)controller.getRecord();
	}
	public List<Business_Plan_Line__c> getcbplan_lines()    {
		//Account acc = [SELECT id FROM Account WHERE id =:account.id];
		List<Business_Plan__c> plan = [SELECT id FROM Business_Plan__c 
			WHERE Account__c = :acc.id
			AND Start_Date__c <= TODAY AND End_Date__c >= TODAY];
		if (plan == null || plan.isEmpty())
			return null;			
		// Business Plan: Start Date e End Date
		// Business Plan Line: Range Start, Range End, Product Range, Revenue, Quantity
		cbplan_lines = [SELECT id, Range_Start__c, Range_End__c, Product_Gamma__c,
		Revenue__c, Quantity__c, Sales_Price__c
			FROM Business_Plan_Line__c
			WHERE Business_Plan__c = :plan[0].id
			AND Range_End__c >= TODAY ORDER BY Range_Start__c ASC];
		
		return cbplan_lines;
	}
	
}